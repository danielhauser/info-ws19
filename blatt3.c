#include <stdio.h>

void exa() {
    printf("Bitte geben Sie 10 chars ein: ");
    char a[10];
    for (int i = 0; i < 10; ++i) {
        scanf("%c", &a[i]);
    }
    for (int j = 0; j < 10; ++j) {
        printf("%c", a[j]);
    }
}

void exb() {
    printf("BItte geben Sie 10 Kleinbuchstaben ein:");
    char kb[10];
    for (int i = 0; i < 10; ++i) {
        char temp;
        scanf("%c", &temp);
        if ((int) temp >= (int) 'a' && (int) temp <= 'z') {
            kb[i] = temp;
        } else {
            printf("Die eingegebenen Chars sind keine Kleinbuchstaben! Wiederholen sie bitte die Eingabe!");

        }

    }
    for (int j = 0; j < 10; ++j) {
        kb[j] = (char) ((int) kb[j] - ('a' - 'A'));
        printf("%c", kb[j]);
    }
}

void exc() {
    //  char wordList[] = {"tarne nie deinen rat"};
    //  0 | 1 | 2 | 3 |
    //  a | b | c | 0 |
    char wordList[] = {"8asdf2s"};
    //position marks the len-1 to avoid the stop char | len = sizeof -1
    int pos = sizeof(wordList) - 2;
    char reverseList[sizeof(wordList)];
    //reverse array order in a temp array
    for (int i = 0; wordList[i] != '\0'; ++i) {
        reverseList[i] = wordList[pos - i];
    }
    //copy temp array in original
    for (int j = 0; j < wordList[j] != '\0'; ++j) {
        wordList[j] = reverseList[j];
    }
    //print
    for (int k = 0; k < wordList[k] != '\0'; ++k) {
        printf("%c", wordList[k]);
    }
}

void exd() {
    char string1[] = {"Das ist der erste "};
    char string2[] = {"und das zweite Teil."};
    int combinedSize= sizeof(string1)+ sizeof(string2)-1;//only one stop char needed
    char stringCombined[combinedSize];
    stringCombined[combinedSize-1]='\0';
    for (int i = 0; string1[i]!='\0' ; ++i) {
        stringCombined[i]=string1[i];
    }
    for (int j = 0; string2[j]!='\0' ; ++j) {
        stringCombined[sizeof(string1)-1+j]=string2[j];
    }
    //print
    for (int k = 0; stringCombined[k]!='\0'; ++k) {
        printf("%c",stringCombined[k]);
    }
}

int main() {
    exd();
    return 0;
}