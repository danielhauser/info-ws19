#include <stdio.h>


void exa() {
    int a[4];
    for (int i = 0; i < 5; ++i) {
        a[i] = 0;
    }
    for (int j = 0; j < 5; ++j) {
        printf("Zahl bitte ");
        scanf("%i", &a[j]);
    }
    for (int k = 0; k < 5; ++k) {
        printf("%d", a[k]);
    }
}

void exb() {
    int a[4];
    for (int i = 0; i < 5; ++i) {
        a[i] = 0;
    }
    for (int j = 0; j < 5; ++j) {
        printf("Zahl bitte ");
        scanf("%i", &a[j]);
    }
    //dubble the values
    for (int l = 0; l < 5; ++l) {
        a[l] *= a[l] * 2;
    }

    for (int k = 0; k < 5; ++k) {
        printf("%d", a[k]);
    }
}

void exc() {
    int a[4];
    int max, min;
    for (int i = 0; i < 5; ++i) {
        a[i] = 0;
    }
    for (int j = 0; j < 5; ++j) {
        printf("Zahl bitte ");
        scanf("%i", &a[j]);
    }
    for (int k = 0; k < 5; ++k) {
        if (a[k] > max) {
            max = a[k];
        }
        if (a[k] < min) {
            min = a[k];
        }
    }

    printf("Maximum: %d Minimum: %d ", max, min);
}

void exd() {
    int a[4];
    int erg = 0;
    for (int i = 0; i < 5; ++i) {
        a[i] = 0;
    }
    for (int j = 0; j < 5; ++j) {
        printf("Zahl bitte ");
        scanf("%i", &a[j]);
    }
    for (int k = 0; k < 5; ++k) {
        erg = erg + a[k];
    }
    printf("Summe: %d \n", erg);
    //teilen durch n=5
    erg /= 5;
    printf("Das Arithmetische Mittel lautet: %d", erg);
}

void exe() {
    int a[3][3];
    int b[3][3];
    int res[3][3];
    int fillin = 1;
    //fill first matrix[x][y]
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            a[i][j] = fillin;
            fillin *= 2;
        }
    }
    fillin = 20;
    for (int l = 0; l < 3; ++l) {
        for (int i = 0; i < 3; ++i) {
            b[l][i] = fillin;
            fillin = fillin - 2;
        }
    }
    //print a
    for (int k = 0; k < 3; ++k) {
        for (int i = 0; i < 3; ++i) {
            printf("%d\t", a[k][i]);
        }
        printf("\n");
    }
    printf("\n\n");
    //print b
    for (int m = 0; m < 3; ++m) {
        for (int i = 0; i < 3; ++i) {
            printf("%d\t", b[m][i]);
        }
        printf("\n");
    }

    //a+b
    for (int n = 0; n < 3; ++n) {
        for (int i = 0; i < 3; ++i) {
            res[n][i] = a[n][i] + b[n][i];
        }

    }
    //print res
    printf("\n\n");
    for (int i1 = 0; i1 < 3; ++i1) {
        for (int i = 0; i < 3; ++i) {
            printf("%d\t", res[i1][i]);
        }
        printf("\n");
    }

}

int main() {
    exe();

    return 0;
}