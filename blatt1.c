#include <stdio.h>
#include <limits.h>
#include <math.h>

/*
 * @author Daniel Hauser
 */

void aufgabea() {
    int i = 0;
    while (i < CHAR_MAX) {
        printf(i);
        i++;
    }
}

void aufgabeb() {
    for (int r = 20; r < 40; ++r) {
        printf("%f", 2 * M_PI * r);
    }
}

void aufgabec() {
    int eingabe = 0;
    printf("Zahl bitte ");
    scanf("%i", &eingabe);
    int i, erg;
    i = 0;
    do {
        erg = erg + i;
        i++;
    } while (i <= eingabe);
    printf("Ergebnis: %d", erg);
}

void aufgabeda() {
    int eingabe = 0;
    int erg = 1;
    printf("Zahl bitte ");
    scanf("%d", &eingabe);

    for (int i = 1; i <= eingabe; ++i) {
        erg = erg * i;
    }
    printf("%d", erg);
}

void aufgabedb() {
    int eingabe = 0;
    int erg = 1;
    int i = 1;
    printf("Zahl bitte");
    scanf("%i", &eingabe);
    while (i <= eingabe) {
        erg = erg * i;
        i++;
    }

    printf("%d", erg);
}

void aufgabee() {
    int eingabea = 0;
    printf("Zahl bitte ");
    scanf("%i", &eingabea);
    int eingabeb = 0;
    printf("Zahl bitte ");
    scanf("%i", &eingabeb);
    if (eingabea <= eingabeb) {
        printf("%d", eingabeb);
    } else {
        printf("%d", eingabea);
    }

}

void rechner() {
    int operation;
    float a,b;
    operation = 0;
    a = 0;
    printf("Zahl bitte ");
    scanf("%f", &a);

    b = 0;
    printf("Zahl bitte ");
    scanf("%f", &b);

    operation = 0;
    printf("Bitte geben Sie die Operation an: [0] Addition, [1] Subtraktion, [2] Multiplikation, [3] Division ");
    scanf("%i", &operation);

    switch (operation){
        case 0:
            printf("%f",a+b);
            break;
        case 1:
            printf("%f",a-b);
            break;
        case 2:
            printf("%f",a*b);
            break;
        case 3:
            if(b!=0){
             printf("%f",a/b);
            } else{
                printf("divided by zero");
            }
            break;
        default:
            printf("Operation nicht erkannt.");
    }

}

int main() {
    rechner();
    return 0;
}
